# Programming Tools and Techniques.

This repository will contain all the material relevant for the
Programming tools and techniques course.

* Source available at: <https://bitbucket.org/ppk-teach/tools/>
* Wiki page:  <https://bitbucket.org/ppk-teach/tools/wiki/>
* Issue tracker: <https://bitbucket.org/ppk-teach/tools/issues/>
* IRC: `irc.freenode.net` channel `##cseiitk-programming-tools` (note
  there are two hashes in the begining)

## How to contribute?

See the wiki page <https://bitbucket.org/ppk-teach/tools/wiki/Contributing>

## Legalese

Copyright 2015 Piyush P Kurur.

The content of this repository is shared under the Creative Commons
Attribution+Share alike license (`./licences/cc-by-sa.txt`). Any
source code of a program or sample code is distributed under the BSD3
license (`./licences/bsd3.txt`).
